FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > wayland.log'

COPY wayland .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' wayland
RUN bash ./docker.sh

RUN rm --force --recursive wayland
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD wayland
